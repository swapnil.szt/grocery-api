import * as mongoose from 'mongoose';
import {
  IsNotEmpty,
  IsEmail,
  IsEmpty,
  IsUrl,
  IsNumber,
  Length,
  IsOptional,
  IsPositive,
  Min,
  Equals,
  IsArray,
  ValidateNested,
  IsString,
  Max,
  IsEnum,
  IsMongoId,
  ArrayMinSize,
  IsBoolean,
  IsDate,
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const UsersSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    email: {
      type: String,
        trim: true,
        lowercase: true,
        unique: true,
    },
    mobileNumber: {
      type: String,
    },
    password: {
      type: String,
    },
    salt: {
      type: String,
    },
    role: {
      type: String,
    },
    // for all role if profile pic required used***********
    profilePic: {
      type: String,
    },
    profilePicId: {
      type: String,
    },
    filePath:{
      type:String
    },
    // **********************
    otp: {
      type: Number,
    },
    playerId:{
      type:String
    },
    mobileNumberverified: {
      type: Boolean,
    },
    emailVerified: {
      type: Boolean,
    },
    verificationId: {
      type: String,
    },
    registrationDate: {
      type: Number,
    },
    status: {
      type: Boolean,
      default:true
    },
    // only for admin
    exportedFile: {
      type: Object,
    },
    // only for admin
    productExportedFile: {
      type: Object,
    },
    //newly added field loyaltyPoint and totalLoyaltyPoints
    loyaltyPoints:{
      type:Array
    },
    //current loyalty point
    totalLoyaltyPoints: {
      type: Number
    },
    // LANGUAGE CODE 
    language: {
      type: String,
    },
    locationId:{
      type: mongoose.Schema.Types.ObjectId,
      ref:'Locations'
    },
    // TOTAL NO OF ORDER CURRENTLY ACCEPTED 
    noOfOrderAccepted: {
      type: Number
    },
    // TOTAL NO OF ORDER DELIVERED
    noOfOrderDelivered: {
      type: Number
    },
    //this is for deliveryBoy
    areaAdminId:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'Users'
    },
    locationArea:
    {
      lat: {
          type: Number,
      },
      long: {
          type: Number,
      },
    },
    personalInfo:
    {
      name:String,
      fatherName:String,
      address:String,
      city:String,
      pincode:String,
      workaddressProof:String,
      PhoneNumber1:Number,
      PhoneNumber2:Number,
      emergencyContact:String,
      bloodGroup:String,
      familyInfo:
      {
        familyMemberNumber:Number,
        maritalStatus:String,
        familyIncome:Number
      },
      bankaccountDetail:
      {
        accountHolderName:String,
        accountType:String,
        accountNumber:String,
        accountBankName:String,
        bankCodeIFC:String,
      },
      identificationDocument:
      {
        adharCardNumber:String,
        adharCardPhoto:String,
        panCardNumber:String,
        panCardPhoto:String,
        voterNumberId:String,
        voteridPhoto:String
      }

    },
    transporterInfo:
    {
      vechileInfo:
      [
        {
          vechileNumberId:String,
          vechileName:String,
          vechileNumber:String,
          vechileType:String,
          vechileCapacity:String,
          vechileImage:String,
          vechileImageId:String,
          vechileFilePath:String,
          vechileFuelType:String,
          vechileMileage:String,
          vechileDetail:String,
          isvechileInInsurance:Boolean,
          vechileinsuranceDetail:String
        }
      ],
      drivingDetailInfo:
      [
        {
          driverName:String,
          driverAge:String,
          driverImageUrl:String,
          driverFilePath:String,
          deriverImageId:String,
          drivingliasenceNumber:String,
          drivingliasenceIssuedBy:String,
          drivingliasenceIssuedDate:String,
          drivingliasenceExpiryDate:String,
          drivingliasenceImageUrl:String,
          drivingliasenceImageId:String,
          drivingliasenceFilePath:String
        }
      ],
      travellingInfo:
      [
        {
          cityTOCity:String,
          pincodes:String,
          travellinghoursTake:String
        }
      ]
    },
    farmerInfo:
    {
      farmerType:String,
      numberOfLands:String,
      landInfo:
      [
        {
          sizeOfLands:String,
          landsLocations:String,
          priceOfLand:String
        }
      ],
      numberOfFamilyMember:String,
      isLandOnLoan:Boolean,
      loanInstallment:String,
      loanIssuedBy:String,
      loanInstallmentTimePeriod:String,
      isLandOnInsurance:Boolean,
      landInsuranceDetail:String,
    },
    traderInfo:
    {
      traderType:String,
      fermName:String,
      fermAddress:String,
      numberOfFerms:String,
      fermInfo:
      [
        {
          sizeOfFerm:String,
          fermLocations:String
        }
      ],
      numberOfFamilyMember:String,
      isFermOnLoan:Boolean,
      loanInstallment:String,
      loanIssuedBy:String,
      loanInstallmentTimePeriod:String,
      isFermOnInsurance:Boolean,
      FermInsuranceDetail:String,
    }
  },
  { timestamps: true },
);

//only These role should be created
enum RoleType
{
  User = 'User',
  DileveryBoy= 'Delivery Boy',
  Admin='Admin',
  Manager='Manager',
  Area_Admin='Area Admin',
  Farmer='Farmer',
  Trader='Trader',
  Transporter='Transporter',
  Coldstoreger='Coldstoreger'
}

enum TraderType
{
  smallTrader='smallTrader',
  mediumTrader='mediumTrader',
  largeTrader='largeTrader'
}

enum FramerType
{
  smallFarmer='smallFarmer',
  mediumFarmer='mediumFarmer',
  largeFarmer='largeTrader'
}

export class identificationDocumentDTO
{
  @IsNotEmpty()
  adharCardNumber:string;

  @IsNotEmpty()
  adharCardPhoto:string;

  @IsNotEmpty()
  panCardNumber:string;

  @IsNotEmpty()
  panCardPhoto:string;

  @IsNotEmpty()
  voterNumberId:String;

  @IsNotEmpty()
  voteridPhoto:String;
}
export class bankaccountDetailDTO
{
  @IsNotEmpty()
  accountHolderName:string;

  @IsNotEmpty()
  accountType:string;

  @IsNotEmpty()
  accountNumber:string;

  @IsNotEmpty()
  accountBankName:string;

  @IsNotEmpty()
  bankCodeIFC:string;
}

export class familyInfoDTO
{
  @IsNotEmpty()
  familyMemberNumber:number;

  @IsNotEmpty()
  maritalStatus:string;

  @IsNotEmpty()
  familyIncome:string;

}

export  class personalInfoDTO
{
  @IsNotEmpty()
  name:string;

  @IsNotEmpty()
  fatherName:string;

  @IsNotEmpty()
  address:string;

  @IsNotEmpty()
  PhoneNumber1:number;

  @IsNotEmpty()
  PhoneNumber2:number;

  @IsNotEmpty()
  emergencyContact:string;

  @IsNotEmpty()
  bloodGroup:string;

  @IsNotEmpty()
  familyInfo:familyInfoDTO;

  @IsNotEmpty()
  bankaccountDetail:bankaccountDetailDTO;

  @IsNotEmpty()
  identificationDocument:identificationDocumentDTO
}

export class  travellingInfoDTO
{
  @IsNotEmpty()
  cityToCity:string;

  @IsNotEmpty()
  pincodes:string;

  @IsNotEmpty()
  travellinghoursTake:string;
}

export class drivingDetailInfoDTO
{
  @IsNotEmpty()
  drivingliasenceNumber:string;

  @IsNotEmpty()
  drivingliasenceIssuedBy:string;

  @IsNotEmpty()
  @IsDate()
  drivingliasenceIssuedDate:string;

  @IsNotEmpty()
  @IsDate()
  drivinngliasenceExpiryDate:string;

  @IsNotEmpty()
  drivingliasenceImageUrl:string;

  @IsNotEmpty()
  drivingliasenceImageId:string;

  @IsNotEmpty()
  divingliasenceFilePath:string
}
export class vechileInfoDTO
{
  @IsNotEmpty()
  vechileNumberId:string;

  @IsNotEmpty()
  vechileName:string;

  @IsNotEmpty()
  vechileNumber:string;

  @IsNotEmpty()
  vechileType:String;

  @IsNotEmpty()
  vechileCapacity:string;

  @IsNotEmpty()
  vechileImage:String;

  @IsNotEmpty()
  vechileImageId:String;

  @IsNotEmpty()
  vechileFilePath:String;

  @IsNotEmpty()
  vechileFuelType:String;

  @IsNotEmpty()
  vechileMileage:String;

  @IsNotEmpty()
  vechileDetail:String;

  @IsNotEmpty()
  isvechileInInsurance:Boolean;

  @IsNotEmpty()
  vechileinsuranceDetail:String

}

export class transporterInfoDTO
{
  @IsNotEmpty()
  vechileInfo:vechileInfoDTO;

  @IsNotEmpty()
  drivingDetailInfo:drivingDetailInfoDTO;

  @IsNotEmpty()
  travellingInfo:travellingInfoDTO;

}

export class landInfoDTO
{
  @IsNotEmpty()
  sizeOfLands:string;
  
  @IsNotEmpty()
  landsLocations:string;

  @IsNotEmpty()
  priceOfLand:string
}

export class farmerInfoDTO
{
  @IsNotEmpty()
  farmerType:String;

  @IsNotEmpty()
  numberOfLands:String;

  @IsNotEmpty()
  landInfo:landInfoDTO;

  @IsNotEmpty()
  numberOfFamilyMember:string;

  @IsNotEmpty()
  isLandOnLoan:boolean;

  @IsNotEmpty()
  loanInstallment:string;

  @IsNotEmpty()
  loanIssuedBy:string;

  @IsNotEmpty()
  loanInstallmentTimePeriod:string;

  @IsNotEmpty()
  isLandOnInsurance:boolean;

  @IsNotEmpty()
  landInsuranceDetail:string;
}

export class fermInfoDTO
{
  @IsNotEmpty()
  sizeOfFerm:string;

  @IsNotEmpty()
  fermLocation:string;
}

export class traderInfoDTO
{
  @IsNotEmpty()
  traderType:string;

  @IsNotEmpty()
  fermName:string;

  @IsNotEmpty()
  fermAddress:string;

  @IsNotEmpty()
  numberOfFerm:string;

  @IsNotEmpty()
  fermInfo:fermInfoDTO;

  @IsNotEmpty()
  numberOfFamilyMember:string;

  @IsNotEmpty()
  isFermOnLoan:boolean;

  @IsNotEmpty()
  loanInstallment:String;

  @IsNotEmpty()
  loanIssuedBy:string;

  @IsNotEmpty()
  loanInstallmentTimePeriod:string;

  @IsNotEmpty()
  isFermOnInsurance:boolean;

  @IsNotEmpty()
  FermInsuranceDetail:string;

}

export class UsersDTO {
  @IsEmpty()
  _id: string;

  @IsNotEmpty()
  personalInfo:personalInfoDTO;

 // @IsOptional()
 // transporterInfo:transporterInfoDTO;

 // @IsOptional()
 // farmerInfo:farmerInfoDTO;

 // @IsOptional()
 // fermInfo:fermInfoDTO;

  @IsOptional()
  @ApiModelProperty()
  firstName: string;

  @IsOptional()
  @ApiModelProperty()
  lastName: string;

  @IsNotEmpty()
  @ApiModelProperty()
  email: string;

  @IsNotEmpty()
  @ApiModelProperty()
  password: string;

  @IsOptional()
  @Length(0, 15)
  @ApiModelProperty()
  mobileNumber: string;

  @IsEmpty()
  salt: string;
  
  @IsOptional()
  playerId:String

  @IsNotEmpty()
  @IsEnum(RoleType, {message: 'Role  type should be User or Admin or DeliveryBoy'})
  @ApiModelProperty()
  role: string;


  @IsOptional()
  otp: number;

  @IsOptional()
  @IsUrl()
  @ApiModelProperty()
  profilePic: string;

  @IsOptional()
  @ApiModelProperty()
  profilePicId: string;

  @IsOptional()
  @ApiModelProperty()
  filePath:string

  @IsOptional()
  registrationDate: number;

  @IsOptional()
  emailVerified: boolean;

  @IsOptional()
  mobileNumberverified: boolean;

  @IsOptional()
  verificationId: string;


  status:boolean
  @IsOptional()
  @ApiModelProperty()
  loyaltyPoints:Array<any>

  @ApiModelProperty()
  totalLoyaltyPoints:number;

  @IsOptional()
  @ApiModelProperty()
  locationId:string
  
  @IsOptional()
  noOfOrderAccepted: number; 

  @IsOptional()
  noOfOrderDelivered: number;
  locationArea:{
    lat: {
        type: Number,
    },
    long: {
        type: Number,
    }
  }
  areaAdminId:string;

}

export class UsersUpdateDTO {
  @IsOptional()
  _id: string;


  @IsNotEmpty()
  personalInfo:personalInfoDTO;

  @IsOptional()
  transporterInfo:transporterInfoDTO;

  @IsOptional()
  farmerInfo:farmerInfoDTO;

  @IsOptional()
  fermInfo:fermInfoDTO;

  @IsOptional()
  @ApiModelProperty()
  firstName: string;

  @IsOptional()
  @ApiModelProperty()
  lastName: string;

  @IsOptional()
  @Length(0, 15)
  @ApiModelProperty()
  mobileNumber: string;


  @IsOptional()
  otp: number;

  @IsOptional()
  @IsUrl()
  @ApiModelProperty()
  profilePic: string;

  @IsOptional()
  @ApiModelProperty()
  profilePicId: string;

  @IsOptional()
  @ApiModelProperty()
  filePath:string

  @IsOptional()
  registrationDate: number;

  @IsOptional()
  emailVerified: boolean;

  @IsOptional()
  mobileNumberverified: boolean;

  @IsOptional()
  verificationId: string;

  @IsOptional()
  @ApiModelProperty()
  playerId:string
  status:boolean

  @IsOptional()
  @ApiModelProperty()
  locationId:string

  locationArea:{
    lat: {
        type: Number,
    },
    long: {
        type: Number,
    }
  }

}



export class CredentialsDTO {
  @IsNotEmpty()
  @IsEmail()
  @ApiModelProperty()
  email: string;
  
  @IsOptional()
  playerId:string

  @IsNotEmpty()
  @ApiModelProperty()
  password: string;
}

export class UploadFileDTO {
  @ApiModelProperty()
  type:string;
}

export class VerifyEmailDTO {
  @IsNotEmpty()
  @IsEmail()
  @ApiModelProperty()
  email: string;
}

export class OTPVerificationDTO {
  @IsNotEmpty()
  @Length(4, 4)
  @ApiModelProperty()
  otp: string;
}

export class PasswordResetDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  password: string;
}

export class ChangePasswordDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  currentPassword: string;

  @IsNotEmpty()
  @ApiModelProperty()
  newPassword: string;

  @IsNotEmpty()
  @ApiModelProperty()
  confirmPassword: string;
}



//deliveryBoy status update model
export class DeliverBoyStatusDTO{
  @IsNotEmpty()
  @ApiModelProperty()
  status:boolean
}

//only for admin
export class ExportedFileDTO{
  @ApiModelProperty()
  exportedFile:object
}
export class PushNotificationDTO{
  @ApiModelProperty()
  @IsNotEmpty()
  title:string
  @ApiModelProperty()
  @IsNotEmpty()
  mssg:String;
  couponecode:string
  
}
