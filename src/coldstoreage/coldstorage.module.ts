import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { PassportModule } from "@nestjs/passport";
import { LocationsSchema } from "../locations/locations.model";
import { ProductsSchema } from "../products/products.model";
import { UploadService } from "../upload/upload.service";
import { UsersSchema } from "../users/users.model";
import { ColdstorageController } from "./coldstorage.controller";
import { ColdstorageSchema } from "./coldstorage.model";
import { ColdstorageService } from "./coldstorage.service";


@Module
(
    {
        imports:
        [
            PassportModule.register({defaultStrategy:'jwt'}),
            MongooseModule.forFeature([
                {name:'Users',schema:UsersSchema},
                {name:'Locations',schema:LocationsSchema},
                {name:'Coldstorage',schema:ColdstorageSchema},
                {name:'Products',schema:ProductsSchema}
            ])
        ],
        controllers:[ColdstorageController],
        providers:[ColdstorageService,UploadService]

    }
)

export class ColdstorageModule
{

}