import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model} from 'mongoose';
import { UploadService } from '../upload/upload.service';
import {UsersDTO} from '../users/users.model';
import {CommonResponseModel} from '../utils/app-service-data';
import {ColdstorageDTO,ColdstorageStausDTO,coldstorageInfoDTO} from './coldstorage.model'

@Injectable()
export class ColdstorageService
{
    constructor
    (
        @InjectModel('Users') private readonly userModel:Model<any>,
        @InjectModel('Locations') private readonly locationModel:Model<any>,
        @InjectModel('Coldstorage') private readonly coldstorageModel:Model<any>,
        @InjectModel('Products') private readonly productModel:Model<any>,
        private utilService:UploadService
    ){}
    
    // create
    public async createColdstorage(user:UsersDTO,coldstorageData:ColdstorageDTO,language:string):Promise<CommonResponseModel>
    {
        try
        {
            if(user.role === 'Coldstoreger')
            {
                console.log("helllo user",user._id);
                coldstorageData.user=user._id;
                const coldstorage=await this.coldstorageModel.create(coldstorageData);
                console.log("9009 coldstorage crate",coldstorage);
                let resMsg=language?await this.utilService.sendResMsg(language,"COLDSTORAGE_SAVED")?await this.utilService.sendResMsg(language,"COLDSTORAGE_SAVED"):"coldstorage saved successfully":'coldsotage saved successfully'
                return { response_code: HttpStatus.CREATED, response_data: {resMsg,coldstorage} };
            }
            else
            {
                let resMsg=language?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE")?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE"):"You are not authorized to access this api":'You are not authorized to access this api'
                return {response_code: HttpStatus.UNAUTHORIZED, response_data: resMsg}; 
            }
        }
        catch(e)
        {
            let resMsg=language?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR")?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR"):"Internal server error":'Internal server error'
            return {response_code: 500, response_data: resMsg}; 
        }
    }

    //update
    public async updateColdstorage(user:UsersDTO,id1:string,id2:string,coldstorageData:coldstorageInfoDTO,language:string):Promise<CommonResponseModel>
    {
        try
        {
            if(user.role==='Coldstoreger')
            {
                const coldstorage= await this.coldstorageModel.findById(id1);
                const coldindex=coldstorage.coldstorageInfo.findIndex(cold=> cold._id ==id2)
                if(coldindex!=-1)
                {
                    coldstorage.coldstorageInfo[coldindex]=coldstorageData;      
                }
                console.log("9008 coldstorageupdate",coldstorage);
                let resMsg=language?await this.utilService.sendResMsg(language,"COLDSTORAGE_UPDATE")?await this.utilService.sendResMsg(language,"COLDSTORAGE_UPDATE"):"coldstorage update successfully":'coldsotage update successfully'
                return { response_code: HttpStatus.CREATED, response_data: {resMsg,coldstorage} };
            }
            else
            {
                let resMsg=language?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE")?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE"):"You are not authorized to access this api":'You are not authorized to access this api'
                return {response_code: HttpStatus.UNAUTHORIZED, response_data: resMsg}; 
            }
        }
        catch(e)
        {
            let resMsg=language?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR")?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR"):"Internal server error":'Internal server error'
            return {response_code: 500, response_data: resMsg}; 
        }
    }

    //statusupdate
    public async updateStatusColdstorage(user:UsersDTO,id:string,coldstorageStatus:ColdstorageStausDTO,language:string):Promise<CommonResponseModel>
    {
        try
        {
            if(user.role==='Coldstoreger')
            {
                const coldstorage= await this.coldstorageModel.findById(id);
                console.log("cold info",coldstorage);
               const coldindex =coldstorage.coldstorageInfo.findIndex(cold=> cold._id==coldstorageStatus.coldId);
               if(coldindex!=-1)
               {
                   coldstorage.coldstorageInfo[coldindex].status=coldstorageStatus.status;
               }
                console.log("9007 coldstoragestatus",coldstorage);
                let resMsg=language?await this.utilService.sendResMsg(language,"COLDSTORAGE_STATUS_UPDATE")?await this.utilService.sendResMsg(language,"COLDSTORAGE_STATUS_UPDATE"):"coldstorage status update  successfully":'coldsotage status update successfully'
                return { response_code: HttpStatus.CREATED, response_data: {resMsg,coldstorage} };

            }
            else
            {
                let resMsg=language?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE")?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE"):"You are not authorized to access this api":'You are not authorized to access this api'
                return {response_code: HttpStatus.UNAUTHORIZED, response_data: resMsg}; 
            }
        }
        catch(e)
        {
            let resMsg=language?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR")?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR"):"Internal server error":'Internal server error'
            return {response_code: 500, response_data: resMsg}; 
        }
    }

    //get Own's coldstorageInfo
    public async getColdstorageInformation(user:UsersDTO,language:string):Promise<CommonResponseModel>
    {
        try
        {
            if(user.role==='Coldstoreger')
            {
                console.log("hello cold",user._id);
                const coldstorageinfo=await this.coldstorageModel.find({user:user._id},'numberOfColdstorage coldstorageInfo');
                console.log("1002 coldinfo",coldstorageinfo);
                if(coldstorageinfo)
                {
                    return {response_code:HttpStatus.OK,response_data:{coldstorageinfo}};
                }
            }
            else
            {
                let resMsg=language?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE")?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE"):"You are not authorized to access this api":'You are not authorized to access this api'
                return {response_code: HttpStatus.UNAUTHORIZED, response_data: resMsg}; 
            }
        }
        catch(e)
        {
            let resMsg=language?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR")?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR"):"Internal server error":'Internal server error'
            return {response_code: 500, response_data: resMsg}; 
        }   
    }
    
}



