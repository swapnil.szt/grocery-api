import * as mongoose from 'mongoose';
import {IsOptional,IsNotEmpty, IsEnum, IsArray, IsNumber, IsMongoId} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';


export const ColdstorageSchema = new mongoose.Schema
(
    {
        
        numberOfColdstorage:
        {
            type:Number
        },
        user:
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:'Users'
        },
        coldstorageInfo:
        [
            {
                coldstorageCode:String,
                coldstorageName:String,
                coldstorageType:String,
                status:
                {
                    type: Number,
                    default: 1
                },
                coldstorageLocation:
                {
                    type:mongoose.Schema.Types.ObjectId,
                    ref:'Locations'
                },
                coldstorageAddress:String,
                coldstoragePincode:String,
                coldstorageContact:Number,
                coldstoragePreservingCommodity:
                [
                    { type: mongoose.Schema.Types.ObjectId,
                        ref: 'Products'
                    }
                ],
                coldstorageLandDetail:
                {
                    coldstorageSize:String,
                    iscoldstorageOnLoan:Boolean,
                    loanInstallment:String,
                    loanIssuedBy:String,
                    loanInstallmentPeriod:String
                }
            }
        ],
        coldId:
        {

            type:mongoose.Schema.Types.ObjectId,
            ref:'Coldstorage'
        }

    }
)


export enum ColdstorageType
{
    large = 'large',
    medium ='medium',
    small ='small'
}

export class coldstorageLandDetailDTO
{
    @IsNotEmpty()
    coldstorageSize:string;

    @IsNotEmpty()
    iscoldstorageOnLoan:boolean;

    @IsNotEmpty()
    loanInstallment:string;

    @IsNotEmpty()
    loanIssuedBy:string;

    @IsNotEmpty()
    loanInstallmentPeriod:string
}

export class coldstorageInfoDTO
{
    @IsNotEmpty()
    coldstorageCode:string;

    @IsNotEmpty()
    coldstorageName:string;

    @IsNotEmpty()
    @IsEnum(ColdstorageType, {message:'enter valid information'})
    coldstorageType:string;

    @IsOptional()
    coldstorageStatus:number;

    @IsNotEmpty()
    coldstorageLocation:string;

    @IsNotEmpty()
    coldstorageAddress:string;

    @IsNotEmpty()
    coldstoragePincode:string;

    @IsNotEmpty()
    coldstorageContact:number;

    @IsNotEmpty()
    @IsArray()
    coldstoragePreservingCommodity:Array<string>;

    @IsNotEmpty()
    coldstorageLandDetail:coldstorageLandDetailDTO
}

export class  ColdstorageDTO
{
    @IsOptional()
    _id:string;

    @IsNotEmpty()
    @ApiModelProperty()
    numberOfColdstorage:number;

    @IsOptional()
    @ApiModelProperty()
    user:String;

    @IsNotEmpty()
    @ApiModelProperty()
    coldstorageInfo:coldstorageInfoDTO

}

export class ColdstorageStausDTO{
    @IsNumber()
    @IsNotEmpty()
    @ApiModelProperty()
    status:number;

    @IsMongoId()
    @IsNotEmpty()
    @ApiModelProperty()
    coldId:string;
}