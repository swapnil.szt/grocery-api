import {Body, Controller, Post, UseGuards,Headers, Put, Param, Get} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UsersDTO } from '../users/users.model';
import { CommonResponseModel } from '../utils/app-service-data';
import { GetUser } from '../utils/user.decorator';
import { ColdstorageDTO,coldstorageInfoDTO, ColdstorageStausDTO } from './coldstorage.model';
import { ColdstorageService } from './coldstorage.service';

@Controller('coldstorages')
export class ColdstorageController
{
    constructor (private coldstorageService:ColdstorageService){}

    // create
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Post('/')
    public createColdstorage(@GetUser() user:UsersDTO,@Body() coldstorageData:ColdstorageDTO,@Headers('language') language:string):Promise<CommonResponseModel>
    {
        return this.coldstorageService.createColdstorage(user,coldstorageData,language);
    }

    //update
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Put('/update/:id1')
    public updateColdstorage(@GetUser() user:UsersDTO,@Param('id1') id1:string,@Headers('id2') id2:string,@Body()coldstorageData:coldstorageInfoDTO,@Headers('language') language:string):Promise<CommonResponseModel>
    {
        return this.coldstorageService.updateColdstorage(user,id1,id2,coldstorageData,language);    
    }

    //statusupdate
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Put('/status/:id')
    public updateStatusColdstorage(@GetUser() user:UsersDTO,@Param('id') id:string,@Body() coldstorageStatus:ColdstorageStausDTO,@Headers('language') language:string):Promise<CommonResponseModel>
    {
        return this.coldstorageService.updateStatusColdstorage(user,id,coldstorageStatus,language);
    }

    //get Own's coldstorageInfo
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Get('/owncoldinfo')
    public getColdstorageInformation(@GetUser() user:UsersDTO,@Headers('language')language:string):Promise<CommonResponseModel>
    {
        return this.coldstorageService.getColdstorageInformation(user,language);                
    }

}

