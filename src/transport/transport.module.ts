import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { PassportModule } from "@nestjs/passport";
import { LocationsSchema } from "src/locations/locations.model";
import { UploadService } from "src/upload/upload.service";
import { UsersSchema } from "src/users/users.model";
import {TransportController} from './transport.controller';
import { TransportSchema } from "./transport.model";
import {TransportService} from './transport.service'



@Module
(
    {
        imports:
        [
            PassportModule.register({defaultStrategy:'jwt'}),
            MongooseModule.forFeature([
                {name:'Users',schema:UsersSchema},
                {name:'Locations',schema:LocationsSchema},
                {name:'Transport',schema:TransportSchema}
            ])
        ],
        controllers:[TransportController],
        providers:[TransportService,UploadService]
    } 
)

export class TransportModule
{

}