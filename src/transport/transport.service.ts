import { HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { UploadService } from "src/upload/upload.service";
import { UsersDTO } from "src/users/users.model";
import { CommonResponseModel } from "src/utils/app-service-data";
import { TransportDTO } from "./transport.model";



@Injectable()
export class TransportService
{
    constructor
    (
        @InjectModel('Users') private readonly userModel:Model<any>,
        @InjectModel('Locations') private readonly locationModel:Model<any>,
        @InjectModel('Transport') private readonly transportModel:Model<any>,
        private utilService:UploadService
    ){}
    
    //create
    public async createTransport(user:UsersDTO,transportData:TransportDTO,language:string):Promise<CommonResponseModel>
    {
        try
        {
            if(user.role ==='Transporter')
            {
                console.log("hello user",user._id);
                transportData.user=user._id;
                const transport = await this.transportModel.create(transportData);
                console.log("9009 transport create",transport);
                let resMsg=language?await this.utilService.sendResMsg(language,"TRANSPORT_SAVED")?await this.utilService.sendResMsg(language,"TRANSPORT_SAVED"):" your transport detail saved successfully":'your transport detail saved successfully'
                return { response_code: HttpStatus.CREATED, response_data: {resMsg,transport} };
            }
            else
            {
                let resMsg=language?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE")?await this.utilService.sendResMsg(language,"UNAUTHORIZED_RESPONSE"):"You are not authorized to access this api":'You are not authorized to access this api'
                return {response_code: HttpStatus.UNAUTHORIZED, response_data: resMsg};
            }
        }
        catch(e)
        {
            let resMsg=language?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR")?await this.utilService.sendResMsg(language,"INTERNAL_SERVER_ERR"):"Internal server error":'Internal server error'
            return {response_code: 500, response_data: resMsg}; 
        }
    }

}