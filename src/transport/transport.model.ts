import { ApiModelProperty } from "@nestjs/swagger";
import { IsDate, IsMongoId, IsNotEmpty, IsNumber, IsOptional } from "class-validator";
import * as  Mongoose from "mongoose";



export const TransportSchema = new Mongoose.Schema
(
    {
        numberOfTransport:
        {
            type:Number
        },
        user:
        {
            type:Mongoose.Schema.Types.ObjectId,
            ref:'Users'
        },
        transporterInfo:
        [{
            transportCode:String,
            transportName:String,
            transportAddress:String,
            transportPincode:Number,
            transportContact:Number,
            transportType:String,
            numberOfTransportVechile:Number,
            status:
            {
                type:Number,
                deafault:1
            },
            transportLocation:
            {
                type:Mongoose.Schema.Types.ObjectId,
                ref:'Locations'
            },
            vechileInfo:
           [
               {
                  vechileCode:String, 
                  vechileNumberId:String,
                  vechileName:String,
                  vechileNumber:String,
                  vechileType:String,
                  vechileCapacity:String,
                  vechileStatus:
                  {
                      type:Number,
                      deafault:1
                  },
                  vechileImage:String,
                  vechileImageId:String,
                  vechileFilePath:String,
                  vechileFuelType:String,
                  vechileMileage:String,
                  vechileDetail:String,
                  isvechileInInsurance:Boolean,
                  vechileinsuranceDetail:String,
                  drivingDetailInfo:
                  [
                      {
                           driverName:String,
                           driverAge:String,
                           driverMobileNumber:Number,
                           driveremail:String,
                           driverHomeAddress:String,
                           driverImageUrl:String,
                           driverFilePath:String,
                           deriverImageId:String,
                           drivingliasenceNumber:String,
                           drivingliasenceIssuedBy:String,
                           drivingliasenceIssuedDate:String,
                           drivingliasenceExpiryDate:String,
                           drivingliasenceImageUrl:String,
                           drivingliasenceImageId:String,
                           drivingliasenceFilePath:String
                       }
                   ],
                   travellingInfo:
                   [
                      {
                         cityTOCity:String,
                         pincodes:String,
                         travellinghoursTake:String
                      }
                  ]                  
               }
            ]
        }]
    }
)


export enum TransportType
{
    Small='Small',
    Medium='Medium',
    Large='Large'
}

export enum VechileType
{
    Small='Small',
    Medium='Medium',
    Large='Large'
}


export class  travellingInfoDTO
{
  @IsNotEmpty()
  cityToCity:string;

  @IsNotEmpty()
  pincodes:string;

  @IsNotEmpty()
  travellinghoursTake:string;
}

export class drivingDetailInfoDTO
{

  @IsNotEmpty()
  driverName:string;

  @IsNotEmpty()
  driverAge:string;

  @IsNotEmpty()
  driverMobileNumber:number;

  @IsNotEmpty()
  driveremail:string;

  @IsNotEmpty()
  driverHomeAddress:string;

  @IsNotEmpty()
  drivingliasenceNumber:string;

  @IsNotEmpty()
  drivingliasenceIssuedBy:string;

  @IsNotEmpty()
  @IsDate()
  drivingliasenceIssuedDate:string;

  @IsNotEmpty()
  @IsDate()
  drivinngliasenceExpiryDate:string;

  @IsNotEmpty()
  drivingliasenceImageUrl:string;

  @IsNotEmpty()
  drivingliasenceImageId:string;

  @IsNotEmpty()
  divingliasenceFilePath:string
}



export class vechileInfoDTO
{
  @IsNotEmpty()
  vechileCode:string;

  @IsNotEmpty()
  vechileNumberId:string;

  @IsNotEmpty()
  vechileName:string;

  @IsNotEmpty()
  vechileNumber:string;

  @IsNotEmpty()
  vechileType:String;

  @IsNotEmpty()
  vechileCapacity:string;

  @IsNotEmpty()
  vechileImage:String;

  @IsNotEmpty()
  vechileImageId:String;

  @IsNotEmpty()
  vechileFilePath:String;

  @IsNotEmpty()
  vechileFuelType:String;

  @IsNotEmpty()
  vechileMileage:String;

  @IsNotEmpty()
  vechileDetail:String;

  @IsNotEmpty()
  isvechileInInsurance:Boolean;

  @IsNotEmpty()
  vechileinsuranceDetail:String

}

export class transporterInfoDTO
{
    @IsNotEmpty()
    transportCode:string;

    @IsNotEmpty()
    transportName:string;

    @IsNotEmpty()
    transportAddress:string;

    @IsNotEmpty()
    transportPincode:number;

    @IsNotEmpty()
    transportType:string;

    @IsNotEmpty()
    numberOfTransportVechile:number;

    @IsNotEmpty()
    status:number;

    @IsNotEmpty()
    location:string;

    @IsNotEmpty()
    vechileInfo:vechileInfoDTO;

    @IsNotEmpty()
    drivingDetailInfo:drivingDetailInfoDTO;

    @IsNotEmpty()
    travellingInfo:travellingInfoDTO;

}

export class  TransportDTO
{
    @IsOptional()
    _id:string;

    @IsNotEmpty()
    @ApiModelProperty()
    numberOfTransport:number;

    @IsOptional()
    @IsMongoId()
    @ApiModelProperty()
    user:string;

    @IsNotEmpty()
    @ApiModelProperty()
    transporterInfo:transporterInfoDTO;
}

export class TransportStatusDTO
{
    @IsNumber()
    @IsNotEmpty()
    @ApiModelProperty()
    status:number;    
}

export class VechileStatusDTO
{
    @IsNumber()
    @IsNotEmpty()
    @ApiModelProperty()
    vechileStatus:number;
}