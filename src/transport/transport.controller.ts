import { Body, Controller, Post, UseGuards,Headers} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth } from "@nestjs/swagger";
import { UsersDTO } from "src/users/users.model";
import { CommonResponseModel } from "src/utils/app-service-data";
import { GetUser } from "src/utils/user.decorator";
import { TransportDTO } from "./transport.model";
import { TransportService } from "./transport.service";




@Controller('transports')
export class TransportController
{
    constructor(private transportService:TransportService){}


    //crate
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Post('/')
    public createTransport(@GetUser() user:UsersDTO,@Body() transportData:TransportDTO,@Headers('language') language:string):Promise<CommonResponseModel>
    {
        return this.transportService.createTransport(user,transportData,language);
    }
}