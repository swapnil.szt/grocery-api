import * as mongoose from 'mongoose';
import {ArrayMinSize, IsArray, IsBoolean, IsEmpty, IsMongoId, IsNotEmpty, IsNumber,Equals ,IsOptional, IsPositive, IsString, Max, Min, ValidateNested, IsEnum} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import {COOrdinatesDTO} from '../address/address.model';
import {Type} from 'class-transformer/decorators';


export const LocationsSchema = new mongoose.Schema({
    locationName: {
        type: String
    },
    address:{
        type: String 
    },
    city: {
        type: String
    },
    postalCode: {
        type: String
    },
    location:
    [
        {
            areaName:String,
            subArea:
            [
                {
                    subAreaName:String,
                    subAreaInfo:
                    [
                        {
                            type:
                            {
                                type:String,
                                default:'Polygon'
                            },
                            subAreaCoordinates:[[[Number]]]
                            
                        }
                    ],
                    storeInfo:
                    [
                    
                        {
                          storeName:String,
                          storeOwnerName:String,
                          phoneNumber:String,
                          storeCoordinates:
                          [
                            {
                                type:
                                {
                                    type:String,
                                    default:'Point'   
                                },
                                coordinates:[Number]
                            }
                          ]
                        }
                    ]
                }
            ]   
        }
    ],
    status: {
        type: Number,
        default: 0
    },
    adminId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    managerId:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
    ],
    areaAdminId:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users'
        },
    ],
    appCode:
    {
        type:String
    }
}, {timestamps: true});


export class storeCoordinatesDTO
{
    @IsNotEmpty()
    @Equals("Point")
    type:string;

    @IsNotEmpty()
    @IsArray()
    coordinates:Array<number>;
}

export class storeInfoDTO
{
    @IsNotEmpty()
    storeName:string;

    @IsNotEmpty()
    storeOwnerName:string;

    @IsNotEmpty()
    phoneNumber:string;

    @IsNotEmpty()
    @IsArray()
    storeCoordinates:storeCoordinatesDTO
}

export class subAreaInfoDTO
{
    @IsNotEmpty()
    @Equals('Polygon')
    type:string;

    @IsNotEmpty()
    @IsArray()
    subAreaCoordinates:Array<Array<Array<number>>>;
}

export class subAreaDTO
{
    @IsNotEmpty()
    subAreaName:string;

    @IsNotEmpty()
    @IsArray()
    subAreaInfo:subAreaInfoDTO;

    @IsNotEmpty()
    @IsArray()
    storeInfo:storeInfoDTO;
}

export class locationDTO
{
    @IsNotEmpty()
    areaName:string;

    @IsNotEmpty()
    subArea:subAreaDTO;

}


export class LocationsDTO {
    @IsNotEmpty()
    @ApiModelProperty()
    locationName: string;

    @IsNotEmpty()
    @ApiModelProperty()
    city: string;

    @IsNotEmpty()
    @ApiModelProperty()
    address: string;

    @IsNotEmpty()
    @IsArray()
    location:locationDTO;

    @IsOptional()
    adminId:string;

    @IsNotEmpty()
    @ApiModelProperty()
    postalCode: string;
    
    @ApiModelProperty()
    managerId:[]

    @IsOptional()
    status: number;
    areaAdminId:[]

    @IsNotEmpty()
    @IsString()
    appCode:string

}

export class LocationStausDTO{
    @IsNumber()
    @IsNotEmpty()
    @ApiModelProperty()
    status :number
}

export class ManagerDTO{
    @IsNotEmpty()
    @ApiModelProperty()
    managerId: Array<string>
}