import * as mongoose from 'mongoose';
import {IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsUrl,IsNumber,IsArray} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export const BannerSchema = new mongoose.Schema({
    title:
    {
        type:String
    },
    description1: 
    {
        type: String
    },
    description2: 
    {
        type: String
    },
    description3: 
    {
        type: String
    },
    description4: 
    {
        type: String
    },
    description5: 
    {
        type: String
    },
    description6: 
    {
        type: String
    },
    description7: 
    {
        type: String
    },
    description8: 
    {
        type: String
    },
    description9: 
    {
        type: String
    },
    description10: 
    {
        type: String
    },
    bannerType: {
        type: String
    },
    imageURL:
    {
        type:String
    },
    filePath:
    {
        type:String
    },
    imageId:
    {
        type:String
    },
    shopInfo:[String],
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Locations',
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categories'
    },
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Products'
    },
    status: {
        type: Number,
        default: 1
    },
    addedBy:
    {
        type:String,
    },
    bannerPriority:
    {
        type:Number
    }
}, {timestamps: true})



export enum BannerType {
    Category = 'Category',
    Product = 'Product',
    Informative="Informative",
    shopType1="shopType1",
    shopType2="shopType2",
    shopType3="shopType3",
    shopType4="shopType4",
    shopType5="shopType5",
    shopType6="shopType6",
    shopType7="shopType7",
    shopType8="shopType8",
    shopType9="shopType9",
    shopType10="shopType10",
    shopType11="shopType11",
    shopType12="shopType12",
    shopType13="shopType13",
    shopType14="shopType14",
    shopType15="shopType15",
    shopType16="shopType16",

}


export class BannerDTO {
    @IsOptional()
    @IsMongoId()
    _id: string;

    @IsNotEmpty()
    @ApiModelProperty()
    title:string;


    @IsOptional()
    @ApiModelProperty()
    description1:string;

    @IsOptional()
    @ApiModelProperty()
    description2:string;

    @IsOptional()
    @ApiModelProperty()
    description3:string;

    @IsOptional()
    @ApiModelProperty()
    description4:string;

    @IsOptional()
    @ApiModelProperty()
    description5:string;

    @IsOptional()
    @ApiModelProperty()
    description6:string;

    @IsOptional()
    @ApiModelProperty()
    description7:string;


    @IsOptional()
    @ApiModelProperty()
    description8:string;


    @IsOptional()
    @ApiModelProperty()
    description9:string;


    @IsOptional()
    @ApiModelProperty()
    description10:string;

    @IsNotEmpty()
    @IsEnum(BannerType)
    @ApiModelProperty()
    bannerType: string;


    @IsNotEmpty()
    @IsUrl()
    @ApiModelProperty()
    imageURL: string;

    @IsNotEmpty()
    @ApiModelProperty()
    imageId: string;

    @IsNotEmpty()
    @ApiModelProperty()
    filePath: string;

    @IsOptional()
    @IsArray()
    shopInfo:Array<string>

    @IsOptional()
    @IsMongoId()
    @ApiModelProperty()
    category: string;

    @IsOptional()
    @IsMongoId()
    @ApiModelProperty()
    location: string;


    @IsOptional()
    @IsMongoId()
    @ApiModelProperty()
    product:string;

    @IsOptional()
    @ApiModelProperty()
    status:number;

    @IsNotEmpty()
    @ApiModelProperty()
    addedBy:string;

    @IsOptional()
    bannerPriority:number;

}

export class bannerStatusDTO
{
    @IsNotEmpty()
    @IsNumber()
    @ApiModelProperty()
    status: number;
}