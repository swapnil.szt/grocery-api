import * as mongoose from 'mongoose';
import {ArrayMinSize, IsArray, IsBoolean, IsEmpty, IsEnum, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString, Max, Min, ValidateNested} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import {Type} from 'class-transformer/decorators';
import {CouponsDTO} from '../coupons/coupons.model';

export const ProductsSchema = new mongoose.Schema({
    title: {
        type: String,
    },
    description: {
        type: String,
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categories',
    },
    // new deal section
    isDealAvailable: {
        type: Boolean,
        default:false
    },
    delaPercent: {
        type: Number
    },
    dealId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Deals'
    },
    subcategory:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Subcategories'
    },
    location:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Locations'
    },
    //******************** */
    addedBy: {
        type: String,
    },
    variant: [
        {
            // title:String,
            productstock: Number,
            unit: String,
            price: Number,
            enable: Boolean,
            MRP:Number,
            offerAmount: Number,
            discount:Number,
            GSTrate:Number,
            GSTamount:Number,
            variantPriority:Number,
            variantSerailNumber:Number
    
        }
    ],
    imageUrl: {
        type: String,
    },
    imageId: {
        type: String,
    },
    filePath:{
        type: String
    },
    status: {
        type: Number,
        default: 1,
    },
    averageRating: {
        type: Number,
    },
    totalRating: {
        type: Number,
    },
    noOfUsersRated: {
        type: Number,
    },
    // NEW FIELD ADDED OPTIONAL(product barcode)
    SKU: {
        type: String,
    },
    productPriority:
    {
        type:Number,
    },
    productSerialNumber:
    {
        type:String
    },
    productBrand:
    {
        type:String,
    },
    productSeries:
    [
        {
            enable:Boolean,
            series:String

        }
    ],
    productType:
    {
        type:String
    }
}, {timestamps: true});

export enum ProductType
{
    GroceryType ='GroceryType',
    CommodityType ='CommodityType'
}

export class ProductsDTO {
    @IsOptional()
    _id: string;

    @IsOptional()
    SKU: string;

    @IsOptional()
    @ApiModelProperty()
    title: string;

    @IsNotEmpty()
    @ApiModelProperty()
    description: string;

    @IsOptional()
    @ApiModelProperty()
    location: string;

    @IsOptional()
    @ApiModelProperty()
    addedBy: string;

    @IsOptional()
    type: String;

    @IsNotEmpty()
    @IsMongoId()
    @ApiModelProperty()
    category: string;

    @ApiModelProperty()
    @ValidateNested({each: true})
    @ArrayMinSize(1)
    @Type(() => VariantDTO)
    variant: VariantDTO[];

    @IsOptional()
    @IsBoolean()
    isDealAvailable: boolean;

    @IsOptional()
    delaPercent: number;

    @IsOptional()
    @IsMongoId()
    dealId: string ;

    @IsNotEmpty()
    @ApiModelProperty()
    imageUrl: string;

    @IsNotEmpty()
    @ApiModelProperty()
    filePath: string;

    @ApiModelProperty()
    productstock: Number;

    @ApiModelProperty()
    unit: String;

    @ApiModelProperty()
    imageId: string;

    @ApiModelProperty()
    subcategory:string
    
    @IsOptional()
    @ApiModelProperty()
    status: number;

    @IsOptional()
    averageRating: number;

    @IsOptional()
    totalRating: number;
     
    @IsOptional()
    noOfUsersRated: number;

    @IsOptional()
    @IsNumber()
    productPriority:number;

    @IsOptional()
    @IsString()
    productSerialNumber:string;

    @IsOptional()
    @IsString()
    productBrand:string;

    @IsOptional()
    @ApiModelProperty()
    @ValidateNested({each: true})
    @ArrayMinSize(1)
    @Type(() => seriesDTO)
    productSeries: seriesDTO[];

    @IsNotEmpty()
    @IsEnum(ProductType,{message:'enter valid producttype'})
    productType:string;
}

export class VariantData {
    // title:String;
    productstock: Number;
    unit: String;
    price: Number;
    MRP:Number
    enable: Boolean;
    offerAmount: number;
    GSTrate:number;
    GSTamount:number;
    variantPriority:number;
    variantSerialNumber:number;
}

export class SeriesData
{
    enable:Boolean;
    series:String
}

export class seriesDTO
{
    //@IsOptional()
    //_id:string;

    @IsOptional()
    @IsBoolean()
    @ApiModelProperty()
    enable:boolean;

    @IsOptional()
    @IsString()
    series:string

}

export class VariantDTO {

     @IsOptional()
     _id:string; 
     
    @IsNotEmpty()
    @IsNumber()
    @ApiModelProperty()
    productstock: number;

    @IsNotEmpty()
    @IsString()
    @ApiModelProperty()
    unit: string;

    @IsNotEmpty()
    @IsNumber()
    @ApiModelProperty()
    price: number;

    @IsNotEmpty()
    @IsBoolean()
    @ApiModelProperty()
    enable: boolean;

    @IsNotEmpty()
    @IsNumber()
    @ApiModelProperty()
    GSTrate:number;

    @IsNotEmpty()
    @IsNumber()
    @ApiModelProperty()
    GSTamount:number;

    @IsOptional()
    @IsNumber()
    variantPriority:number;

    @IsOptional()
    @IsString()
    variantSerialNumber:number;
}

export class PuductStatusDTO {
    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    @Max(1)
    @ApiModelProperty()
    status: number;
}

export class DealProductDTO {
    @IsOptional()
    delaPercent: number;

    @IsOptional()
    isDealAvailable: boolean;

    @IsOptional()
    @IsMongoId()
    dealId: string 
}

